<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyUsersTable extends Migration
{

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('my_users', function (Blueprint $table)
    {
      $table->increments('id');
      $table->string('email');
      $table->string('password');
      $table->string('name');
    });

    $firstnames = [
      'Jack',
      'John',
      'Ian',
      'Brian',
      'Luke',
      'Sam',
      'Jason',
    ];
    $familynames = [
      'Smith',
      'Taylor',
      'Stone',
      'Woods',
      'Snow',
    ];
    for ( $i = 0; $i < 30; $i++ )
    {
      $firstname = $firstnames[rand(0, count($firstnames)-1)];
      $familyname = $familynames[rand(0, count($familynames)-1)];
      $name = $firstname . ' ' . $familyname;
      $email = $firstname . '.' . $familyname . '@example.com';
      $pass = $this->genPass();

      DB::table('my_users')->insert(
        array(
          'email' => $email,
          'password' => $pass,
          'name' => $name,
        )
      );
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('my_users');
  }

  private function genPass($length = 8)
  {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array();
    $alphaLength = strlen($alphabet) - 1;
    for ( $i = 0; $i < $length; $i++ )
    {
      $n = rand(0, $alphaLength);
      $pass[] = $alphabet[$n];
    }
    return implode($pass);
  }

}
