<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use App\myUser;
use Maatwebsite\Excel\Facades\Excel;
use function app;

class XLSController extends Controller
{

  public function export()
  {
    $users = myUser::all()->toArray();
    Excel::create('Users', function($excel) use ($users) {
      $excel->sheet('Users', function($sheet) use ($users) {
        $sheet->fromArray($users);
      });
    })->export('xlsx');
  }
}
